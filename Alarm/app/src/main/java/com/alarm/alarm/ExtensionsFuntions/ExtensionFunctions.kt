package com.alarm.alarm.ExtensionsFuntions

import android.app.Activity
import android.content.Intent
import android.widget.Toast

fun Activity.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT){
    Toast.makeText(this, message,duration).show()
}

inline fun <reified T : Activity>Activity.goToActivity(noinline init: Intent.() -> Unit = {}){

    val intent = Intent(this, T::class.java)
    intent.init()
    startActivity(intent)
}