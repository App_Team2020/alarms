package com.alarm.alarm.Retrofit.Model.GetData

class getDataResult(
    var id: String,
    var Nombre: String,
    var Apellidos: String,
    var Tipo_de_usuario: String,
    var Email: String,
    var Telefono: String,
    var Imagen: String,
    var Sexo: String,
    var Calificación: String,
    var Status: String,
    var id_vehiculo: String,
    var Placa: String,
    var Modelo: String,
    var Año: String,
    var Color: String,
    var Num_Pasajeros: String,
    var idModoTrabajo: String,
    var idCiudad: String,
    var EmailStatus: String,
    var idZona: String,
    var Ganancia_Diaria: String
)
