package com.alarm.alarm.Activities.Registry

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.alarm.alarm.ExtensionsFuntions.toast
import com.alarm.alarm.R
import kotlinx.android.synthetic.main.activity_registry.*

class Registry_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registry)

        btnRegistry.setOnClickListener {
            toast("registry user")
        }
    }

    fun registry(user: String, password: String, phone: String){

        if (user.isNotEmpty() && password.isNotEmpty() && phone.isNotEmpty()){

        }else {
            if (!user.isNotEmpty()){
                toast("Llena el campo usuario.")
                return
            }

            if (!password.isNotEmpty()){
                toast("Llena el campo contraseña.")
                return
            }

            if (!phone.isNotEmpty()){
                toast("Llena el campo telefono.")
                return
            }
        }
    }
}
