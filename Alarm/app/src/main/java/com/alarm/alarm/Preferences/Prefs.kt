package com.alarm.alarm.Preferences

import android.content.Context
import android.content.SharedPreferences


class Prefs (context: Context) {
    val PREFS_NAME = "login.preferences"
    val SHARED_NAME = "sharedLoginPreferences"
    val prefs: SharedPreferences = context.getSharedPreferences(PREFS_NAME, 0)

    var isLogin: Boolean
        get() = prefs.getBoolean(SHARED_NAME, false)
        set(value) = prefs.edit().putBoolean(SHARED_NAME, value).apply()
}