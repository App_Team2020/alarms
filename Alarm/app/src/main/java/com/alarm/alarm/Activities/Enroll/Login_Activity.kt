package com.alarm.alarm.Activities.Enroll

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.alarm.alarm.Activities.Registry.Registry_Activity
import com.alarm.alarm.ExtensionsFuntions.toast
import com.alarm.alarm.R
import com.alarm.alarm.Retrofit.Model.GetData.GetDataResponse
import com.alarm.alarm.Retrofit.Model.LoginResponse
import com.alarm.alarm.Retrofit.RetrofitModule
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Login_Activity : AppCompatActivity() {

    var retrofitModule: RetrofitModule = RetrofitModule()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        button_Login.setOnClickListener {
            login(
            edtTNameUser.text.toString(),
            edtTPassword.text.toString()
            )
        }

        btnSendToRegistry.setOnClickListener {
            val intent = Intent(this, Registry_Activity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
            finish()
        }
    }

    private fun login(phone: String, password: String){

        if (phone.isNotEmpty() && password.isNotEmpty()){
            retrofitModule.getLoginRequest()
                .Login(phone,password,"2")
                .enqueue(object: Callback<LoginResponse>{
                override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                    toast("Error")
                }

                override fun onResponse(
                    call: Call<LoginResponse>,
                    response: Response<LoginResponse>
                ) {
                    if(response.isSuccessful){
                        if (response.body()!!.response){
                            val token = response.body()!!.result.token
                            getData(token)
                        }else{
                            val message = response.body()!!.message
                            toast(message)
                        }
                    }
                }

            })

        }else{
            if (!phone.isNotEmpty()){
                toast("Llena el campo nombre de usuario")
                return
            }

            if (!password.isNotEmpty()){
                toast("Llena el campo contraseña")
                return
            }
        }
    }

    fun getData(tokenUser: String){

        retrofitModule.getDataRequest().getData(tokenUser).enqueue(object: Callback<GetDataResponse>{

            override fun onFailure(call: Call<GetDataResponse>, t: Throwable){
                toast("Error de servidor")
            }

            override fun onResponse(
                call: Call<GetDataResponse>,
                response: Response<GetDataResponse>
            ) {
                if(response.isSuccessful){
                    if (response.body()!!.response){
                        val result = response.body()!!.result
                        toast(result.Nombre)

                    }else{
                        val message = response.body()!!.message
                        toast(message)
                    }
                }
            }
        })
    }
}
