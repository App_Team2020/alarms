package com.alarm.alarm.Retrofit.Model.Registry

class RegistryResponse(
    var result: ResultRegistry,
    var response: String,
    var message: String,
    var errors: String
)