package com.alarm.alarm.Retrofit

import android.util.Log
import com.alarm.alarm.Retrofit.Model.GetData.GetDataResponse
import com.alarm.alarm.Retrofit.Model.LoginResponse
import com.alarm.alarm.Retrofit.Model.Registry.RegistryResponse
import retrofit2.Call
import retrofit2.http.*

interface AlarmaApi {

    interface Login{
        @POST("auth/autenticar")
        @FormUrlEncoded
        fun Login(@Field("Telefono") telefono: String,
                  @Field("Password") password: String,
                  @Field("Tipo_usuario") tipoUsuario: String): Call<LoginResponse>
    }

    interface getData{
        @GET("auth/getData/{tokenUser}")
        fun getData(@Path("tokenUser") tokenUser:String): Call<GetDataResponse>
    }

    interface registry{
        @POST("persona/registrarUsuario")
        @FormUrlEncoded
        fun Registry(@Field("Nombre") name:String): Call<RegistryResponse>
    }
}