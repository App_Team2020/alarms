package com.alarm.alarm.Retrofit.Model

class LoginResponse(

    var result: ResultLogin,
    var response: Boolean,
    var message: String,
    var errors: String
)