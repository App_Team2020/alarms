package com.alarm.alarm.Retrofit.Model.GetData

class GetDataResponse(

    var result: getDataResult,
    var response: Boolean,
    var message: String,
    var errors: String
)