package com.alarm.alarm

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.alarm.alarm.Activities.Enroll.Login_Activity
import kotlinx.android.synthetic.main.activity_home.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val intent = Intent(this, Login_Activity::class.java)
        startActivity(intent)

        /*btnMenu.setOnClickListener {

        }*/
    }
}
