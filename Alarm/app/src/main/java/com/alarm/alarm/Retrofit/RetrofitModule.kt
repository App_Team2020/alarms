package com.alarm.alarm.Retrofit

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class RetrofitModule {

    val ROOT = "http://lubo.com.mx/ApiRESTdev/public/"

    val interceptor = HttpLoggingInterceptor()
    val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

    //val interceptor = HttpLoggingInterceptor()
    //interceptor.level = HttpLoggingInterceptor.Level.BODY
    //val client = OkHttpClient.Builder().addInterceptor(interceptor).build()

    fun getLoginRequest() : AlarmaApi.Login{
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(ROOT)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return  retrofit.create<AlarmaApi.Login>(AlarmaApi.Login::class.java)
    }

    fun getDataRequest(): AlarmaApi.getData{
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(ROOT)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return  retrofit.create<AlarmaApi.getData>(AlarmaApi.getData::class.java)
    }

    fun getRegistryRequest(): AlarmaApi.registry{
        val retrofit: Retrofit = Retrofit.Builder()
            .baseUrl(ROOT)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return  retrofit.create<AlarmaApi.registry>(AlarmaApi.registry::class.java)
    }
}