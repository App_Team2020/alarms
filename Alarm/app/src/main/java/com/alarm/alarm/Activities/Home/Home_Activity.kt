package com.alarm.alarm.Activities.Home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.alarm.alarm.ExtensionsFuntions.toast
import com.alarm.alarm.R
import kotlinx.android.synthetic.main.activity_home.*

class Home_Activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

       btnSendHelp.setOnLongClickListener {
           toast("send Push to contacts")
           return@setOnLongClickListener true
       }
    }
}
