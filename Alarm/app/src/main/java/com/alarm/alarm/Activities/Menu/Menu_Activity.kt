package com.alarm.alarm.Activities.Menu

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.alarm.alarm.Activities.Profile.ChangeImageProfile.Change_Image_Profile_Activity
import com.alarm.alarm.Activities.Profile.Profile_Activity
import com.alarm.alarm.ExtensionsFuntions.toast
import com.alarm.alarm.R
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_home.btnSendHelp
import kotlinx.android.synthetic.main.activity_menu.*
import kotlinx.android.synthetic.main.content_menu_.*


class Menu_Activity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
//    private lateinit var appBarConfiguration: AppBarConfiguration


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        setNavigationViewListener()

        fabMenu.setOnClickListener{
            drawer_layout.openDrawer(GravityCompat.START)
        }

        btnSendHelp.setOnLongClickListener {
            toast("send to push")
            return@setOnLongClickListener true
        }

    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when(item.itemId){

            R.id.itemGoToProfile ->{
                startActivity(Intent(this, Change_Image_Profile_Activity::class.java))
            }

            R.id.itemGoToContact ->{

                toast("Go to itemGoToContact")
                //startActivity(Intent())
            }
            R.id.itemGoToTravel ->{
                toast("Go to itemGoToTravel")
            }
            R.id.itemGoToConfiguration ->{
                toast("Go to itemGoToConfiguration")
            }

        }

        //close navigation drawer
        drawer_layout.closeDrawer(GravityCompat.START)

        return true
    }

    private fun setNavigationViewListener() {
        val navigationView = findViewById<View>(R.id.nav_view) as NavigationView
        navigationView.setNavigationItemSelectedListener(this)
    }

    /*
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }*/
}
